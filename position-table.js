import {
  html, PolymerElement
}
from '@polymer/polymer/polymer-element.js';

import '@polymer/paper-styles/paper-styles.js';
import '@polymer/iron-image/iron-image.js';
import '@vaadin/vaadin-grid/vaadin-grid.js';

/**
 * `position-table`
 * Statistics about teams ina tournament
 *
 * @customElement
 * @polymer
 * @demo demo/index.html
 */
class PositionTable extends PolymerElement {
  static get template() {
    return html `
      <style>
        :host {
          display: block;
        }
        
        .horizontal {
          @apply --layout-horizontal;
        }
        
        .vertical {
          @apply --layout-vertical;
        }
        
        .center {
          @apply --layout-center-justified;
        }
        
        .position {
          @apply --paper-font-body1;
          color: var(--paper-grey-600);
        }
        
        .club {
          @apply --paper-font-subhead;
          color: var(--paper-grey-900);
        }
        
        iron-image {
          width: 30px;
          height: 30px;
        }
        
        .margin-l {
          margin-left: 5px;
        }
        
      </style>
      
      <!-- The array is set as the <vaadin-grid>'s "items" property -->
      <vaadin-grid aria-label="Basic Binding Example" items="[[clubs]]">
  
        <vaadin-grid-column flex-grow="0" width="45px">
          <template class="header">Club</template>
          <template>
            <div class="horizontal center">
              <span class="position">[[_getPosition(index)]]</span>
            </div>
          </template>
        </vaadin-grid-column>
  
        <vaadin-grid-column width="40px">
          <template class="header"></template>
          <template>
            <div class="vertical center">
              <iron-image src="[[item.image]]" sizing="cover"></iron-image>
            </div>
          </template>
        </vaadin-grid-column>
        
        <vaadin-grid-column>
          <template class="header"></template>
          <template>
              <span class="club">[[item.name]]</span>
            </div>
          </template>
        </vaadin-grid-column>
  
        <vaadin-grid-column width="30px">
          <template class="header">PJ</template>
          <template>
            <span class="position">[[item.jj]]</span>
          </template>
        </vaadin-grid-column>
        
        <vaadin-grid-column width="30px">
          <template class="header">G</template>
          <template>
            <span class="position">[[item.jg]]</span>
          </template>
        </vaadin-grid-column>
        
        <vaadin-grid-column width="30px">
          <template class="header">E</template>
          <template>
            <span class="position">[[item.je]]</span>
          </template>
        </vaadin-grid-column>
        
        <vaadin-grid-column width="30px">
          <template class="header">P</template>
          <template>
            <span class="position">[[item.jp]]</span>
          </template>
        </vaadin-grid-column>
        
        <vaadin-grid-column width="30px">
          <template class="header">GF</template>
          <template>
            <span class="position">[[item.gf]]</span>
          </template>
        </vaadin-grid-column>
        
        <vaadin-grid-column width="30px">
          <template class="header">GC</template>
          <template>
            <span class="position">[[item.gc]]</span>
          </template>
        </vaadin-grid-column>
        
        <vaadin-grid-column width="30px">
          <template class="header">DG</template>
          <template>
            <span class="position">[[item.dif]]</span>
          </template>
        </vaadin-grid-column>
        
        <vaadin-grid-column width="45px">
          <template class="header">Pts</template>
          <template>
            <span class="position">[[item.pts]]</span>
          </template>
        </vaadin-grid-column>
  
      </vaadin-grid>
    `;
  }
  static get properties() {
    return {
      clubs: {
        type: Array,
        value: [{
          name: 'Cruz Azul',
          image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Escudo_del_Cruz_Azul_AC.svg/1200px-Escudo_del_Cruz_Azul_AC.svg.png',
          jj: 10,
          jg: 7,
          je: 2,
          jp: 1,
          gf: 17,
          gc: 5,
          dif: 12,
          pts: 23
        }, {
          name: 'America',
          image: 'https://www.clubamerica.com.mx/portal/wp-content/themes/cfamerica/img/header/ClubAmericaLogo.png',
          jj: 10,
          jg: 6,
          je: 2,
          jp: 2,
          gf: 20,
          gc: 11,
          dif: 9,
          pts: 20
        }]
      }
    };
  }
  
  _getPosition(value){
    return parseFloat(value) + 1;
  }
}

window.customElements.define('position-table', PositionTable);